drbd = node['gitlab-drbd']['drbd']
build_path = "#{Chef::Config[:file_cache_path]}/drbd"

# Default to the DRBD version reported by the kernel module
drbd_version = drbd['version'] || `awk '/^version:/ { print $2 }' /proc/drbd`.chomp

include_recipe 'gitlab-drbd::build_deps'


bash "compile DRBD kernel module" do
  code <<-EOH
    set -e # Abort the compile script if any step fails
    ./autogen.sh
    ./configure --prefix=#{drbd['prefix']} --localstatedir=#{drbd['localstatedir']} --sysconfdir=#{drbd['sysconfdir']} --with-km
    (cd drbd && make clean all install)
  EOH

  cwd build_path
  not_if "ls /lib/modules/$(uname -r)/updates/drbd.ko"
end

