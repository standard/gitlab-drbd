drbd = node['gitlab-drbd']['drbd']
build_path = "#{Chef::Config[:file_cache_path]}/drbd"

# Default to the DRBD version reported by the kernel module
drbd_version = drbd['version'] || `awk '/^version:/ { print $2 }' /proc/drbd`.chomp

%w{git-core autoconf gcc flex make xsltproc}.each do |pkg|
  package pkg
end

git build_path do
  repository drbd['repository']
  revision "drbd-#{drbd_version}"
end
