ruby_block "add drbd to /etc/modules" do
  block do
    modules = Chef::Util::FileEdit.new('/etc/modules')
    modules.insert_line_if_no_match(/^drbd$/, 'drbd')
    modules.write_file
  end
end

execute "modprobe drbd" do
  not_if "lsmod | grep '^drbd '"
end
