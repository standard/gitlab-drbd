bag = node['gitlab-drbd']['resources']['data_bag']

data_bag(bag).each do |resource_item|
  resource = data_bag_item(bag, resource_item)

  log resource.inspect do
    level :debug
  end

  template "/etc/drbd.d/#{resource['id']}.res" do
    source "drbd_resource.erb"
    variables resource.to_hash

    not_if { resource['on'][node['hostname']].nil? }
  end
end

global_common_conf = '/etc/drbd.d/global_common.conf'
ruby_block "stop DRBD from phoning home" do
  block do
    drbd_conf = Chef::Util::FileEdit.new(global_common_conf)
    drbd_conf.search_file_replace_line(/usage-count yes;/, 'usage-count no;')
    drbd_conf.write_file
  end

  only_if "grep 'usage-count yes' #{global_common_conf}"
end
