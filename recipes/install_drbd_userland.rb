drbd = node['gitlab-drbd']['drbd']
build_path = "#{Chef::Config[:file_cache_path]}/drbd"

# Default to the DRBD version reported by the kernel module
drbd_version = drbd['version'] || `awk '/^version:/ { print $2 }' /proc/drbd`.chomp

include_recipe 'gitlab-drbd::build_deps'

bash "compile DRBD utilities" do
  code <<-EOH
    set -e # Abort the compile script if any step fails
    ./autogen.sh
    ./configure --prefix=#{drbd['prefix']} --localstatedir=#{drbd['localstatedir']} --sysconfdir=#{drbd['sysconfdir']}
    make clean
    make
    make doc
    make install
  EOH

  cwd build_path
  not_if "drbdadm --version | grep 'DRBDADM_VERSION=#{drbd_version}'"
  notifies :enable, 'service[drbd]'
end

service "drbd" do
  init_command '/etc/init.d/drbd'
end
