# Package psmisc provides the `fuser` command
package 'psmisc'

# We need the iputils-arping package, not arping, for the right `arping`
# command.
package 'iputils-arping'

script_path = '/opt/gitlab-drbd/bin/gitlab-drbd'

directory File.dirname(script_path) do
  recursive true
end

cookbook_file 'gitlab-drbd' do
  path script_path
  mode 0755
end

link '/usr/bin/gitlab-drbd' do
  to script_path
end

template '/etc/gitlab-drbd.conf' do
  variables node['gitlab-drbd']['control_script'].to_hash
end

directory node['gitlab-drbd']['control_script']['drbd_mountpoint'] do
  recursive true
end
