# gitlab-drbd Cookbook

GitLab DRBD configuration and control script.

The recipes in this cookbook can be used to maintain a basic DRBD configuration
and to install the `gitlab-drbd` control script. If you are not using Chef,
please consider this repository as a source of documentation. Places of
interest are [the doc directory](doc/), the [control script
itself](files/default/gitlab-drbd) and the [control script configuration
template](templates/default/gitlab-drbd.conf.erb).

## gitlab-drbd script documentation

See [doc/control_script.md](doc/control_script.md).

## GitLab.com Emergency checklist

See [doc/emergency_checklist.md](doc/emergency_checklist.md).

## Requirements

This cookbook is developed for Ubuntu 12.04.

## Usage

### `gitlab-drbd::install`

Install the DRBD kernel module, userland tools and control script.

### `gitlab-drbd::resources`

Render the resources defined in data bag `node['gitlab-drbd']['resources']['data_bag']`.

Example resource data bag item:

```json
{
  "id": "gitlab_data",
  "device_minor": "0",
  "on": {
    "node1": {
      "address": "192.168.0.101",
      "port": "7789",
      "disk": "/dev/gitlab_vg/drbd"
    },
    "node2": {
      "address": "192.168.0.102",
      "port": "7789",
      "disk": "/dev/gitlab_vg/drbd"
    }
  }
}
```

Note that this recipe does not take care of the initialization of your DRBD
resource; see [the DRBD documentation](doc/set_up_drbd.md).
