# GitLab.com emergency checklist

This document contains troubleshooting steps for dealing with a GitLab.com
availability incident. You can use it as an example for other systems.

## 1. Look for the server server that is up-to-date:

- `ssh linus.gitlab.com cat /proc/drbd`
- `ssh monty.gitlab.com cat /proc/drbd`

## 2. If both are up to date and one of them is master

- `ssh linus/monty.gitlab.com`
- `sudo gitlab-ctl status`
- `sudo gitlab-drbd status`
- `htop`
- if the system seems overloaded due to extreme demand consider turning off the
  services for a few minutes: `sudo gitlab-ctl stop`


## 3. If both are up to date and both are slaves

- make one of them master

## 4. If one can't be reached but the other is up to date:

- reset the unreachable server via the IPMI
- make to the uptodate server master

## 5. If one is up to date but the other is out of date:

- ensure the out of date server becomes slave (IPMI/sudo reboot)
- failover to the uptodate server
