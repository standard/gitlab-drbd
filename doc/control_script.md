# GitLab-DRBD control script

The `gitlab-drbd` script is used to perform manual failovers in a DRBD-based
Master/Slave GitLab cluster.

## Prerequisites

We assume we have a DRBD resource `gitlab_data` hosted on nodes `node1` and
`node2` and a floating IP address 1.2.3.4 that can be bound to network device
`eth0` on either `node1` or `node2`. The `gitlab_data` resource contains an
Ext4 filesystem. We assume that `gitlab.example.com` is a DNS record pointing to
1.2.3.4, the floating IP.

## Initial setup

### Set up omnibus-gitlab

The contents of `/etc/gitlab/` and the installed version of the GitLab omnibus
package need to be (and stay!) identical on node1 and node2. We recommend that
you use a configuration management system for this, e.g.
[Chef](https://gitlab.com/gitlab-org/cookbook-omnibus-gitlab). The gitlab-drbd
control script does not depend on Chef however; use whatever works for you and
your organization.

Configure omnibus-gitlab services with `ha` set to true. This prevents the
services from starting at boot.

```ruby
postgresql['ha'] = true
unicorn['ha'] = true
sidekiq['ha'] = true
redis['ha'] = true
nginx['ha'] = true
remote_syslog['ha'] = true
```

In addition, we enable the omnibus-gitlab mountpoint check to prevent services
from starting unless a filesystem is mounted at `/var/opt/gitlab`. This setting
will disrupt the bootstrap process, so we disable it.

```ruby
high_availability['mountpoint'] = "/var/opt/gitlab"
bootstrap['enable'] = false
```

Define numerical UIDs and GIDs for all omnibus-gitlab users in
`/etc/gitlab/gitlab.rb`: because we will share a filesystem between two hosts,
the (numerical) file owners must align on both hosts.

```ruby
user['uid'] = 1100
user['gid'] = 1100
postgresql['uid'] = 1101
postgresql['gid'] = 1101
redis['uid'] = 1102
redis['gid'] = 1102
web_server['uid'] = 1103
web_server['gid'] = 1103
```

Install omnibus-gitlab on both nodes with these settings. Note that you may see
an error while `gitlab-ctl reconfigure` tries to clear the cache or migrate the
database; this error can be ignored.

On one node, make DRBD the primary, mount the resource at `/var/opt/gitlab`,
and perform the following bootstrap steps:

```
sudo gitlab-ctl reconfigure # write configuration files to the production filesystem
sudo gitlab-ctl start # start gitlab services
sudo gitlab-ctl reconfigure # initialize the database
```

At this point a fresh GitLab instance should be running on your DRBD primary
node. You can log in with `root` / `5iveL!fe`.

### Configuring gitlab-drbd

Gitlab-drbd reads its settings from `/etc/gitlab-drbd.conf`. You can ignore the
ERB tags (`<%= ... %>`) in the [example configuration
file](../templates/default/gitlab-drbd.conf.erb) if you are not using Chef.

## Failover procedure

> __Never issue gitlab-drbd commands through a SSH connection to the floating
  IP.__ If node1 is master and you log in to node1 using `ssh gitlab.example.com` (i.e.
through the floating IP) running `gitlab-drbd slave` will drop your connection
when the IP is dissociated from node1. Avoid this problem by always logging in
to the node directly, e.g. `ssh node1`. To find the hostname of the current
master, run `ssh gitlab.example.com hostname -f`.

A failover consists of two steps. First we demote the current master to the
slave role. After that we promote one of the two slaves to master.

### Demote the current master to slave

There are two ways to demote a node to slave. The gentle way is to use the
`gitlab-drbd slave` command. The rough way is to reboot the node: the default
role after boot is the slave role.

We will demonstrate the gentle way. We can see node1 is master:

```
admin@node1:~$ sudo gitlab-drbd status
(node1) IP address:
    inet 1.2.3.4/25 scope global secondary eth0
(node1) DRBD primary:
Primary/Secondary
(node1) DRBD mounted:
/dev/drbd0 on /var/opt/gitlab type ext4 (rw)
(node1) Service statuses:
run: nginx: (pid 3210) 1178s, normally down; run: log: (pid 1743) 1432s
run: postgresql: (pid 3228) 1177s, normally down; run: log: (pid 1749) 1432s
run: redis: (pid 3230) 1177s, normally down; run: log: (pid 1754) 1432s
run: sidekiq: (pid 3240) 1176s, normally down; run: log: (pid 1744) 1432s
run: unicorn: (pid 3243) 1176s, normally down; run: log: (pid 1752) 1432s
```

Now we tell node1 to become slave.

```
admin@node1:~$ sudo gitlab-drbd slave
(node1) Are you sure you want to stop GitLab, become DRBD secondary and release IP 1.2.3.4 on node1?
(node1) Enter 'Yes' to continue
Yes
(node1) Demoting to slave
(node1) ... Stopping GitLab services
ok: down: nginx: 0s
ok: down: postgresql: 0s
ok: down: redis: 0s
ok: down: sidekiq: 0s
ok: down: unicorn: 0s
(node1) ... Unmount /var/opt/gitlab
(node1) ... Becoming DRBD secondary for gitlab_test
(node1) ... Releasing IP 1.2.3.4/25
(node1) node1 is now slave
```

Look for the `(node1) node1 is now slave` message at the end of the
`gitlab-drbd slave` run. If you do not see this message at the end, the
transition to slave is not finished yet. Retry `gitlab-drbd slave` a few times.
If all else fails, consider rebooting the server you are trying to demote.

### Promote a slave to master

We assume node2 is in the slave state.

```
admin@node2:~$ sudo gitlab-drbd status
(node2) IP address:
(node2) ... no
(node2) DRBD primary:
(node2) ... no
(node2) DRBD mounted:
(node2) ... no
(node2) Service statuses:
down: nginx: 285s; run: log: (pid 1743) 1887s
down: postgresql: 285s; run: log: (pid 1749) 1887s
down: redis: 284s; run: log: (pid 1754) 1887s
down: sidekiq: 284s; run: log: (pid 1744) 1887s
down: unicorn: 283s; run: log: (pid 1752) 1887s
```

We promote it to master with `gitlab-drbd master`.

```
admin@node2:~$ sudo gitlab-drbd master
(node2) Are you sure you want to become DRBD primary, acquire IP 1.2.3.4 and start GitLab on node2?
(node2) Enter 'Yes' to continue
Yes
(node2) Promoting to master
(node2) ... Becoming DRBD primary for gitlab_test
(node2) ... Acquiring IP 1.2.3.4/25
(node2) ... Mounting DRBD resource gitlab_test at /var/opt/gitlab
(node2) ... Starting GitLab services
ok: run: nginx: (pid 5426) 1s, normally down
ok: run: postgresql: (pid 5444) 0s, normally down
ok: run: redis: (pid 5446) 1s, normally down
ok: run: sidekiq: (pid 5456) 0s, normally down
ok: run: unicorn: (pid 5460) 1s, normally down
(node2) ... Broadcasting ownership of 1.2.3.4 via ARP
(node2) node2 is now master
```

Note that thanks to DRBD, we can only promote one node to master. One node1
`gitlab-drbd master` aborts because node2 is already DRBD primary.

```
admin@node1:~$ sudo gitlab-drbd master
(node1) Are you sure you want to become DRBD primary, acquire IP 1.2.3.4 and start GitLab on node1?
(node1) Enter 'Yes' to continue
Yes
(node1) Promoting to master
(node1) ... Becoming DRBD primary for gitlab_test
0: State change failed: (-1) Multiple primaries not allowed by config
Command 'drbdsetup primary 0' terminated with exit code 11
admin@node1:~$
```
