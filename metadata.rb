name             'gitlab-drbd'
maintainer       'GitLab BV'
maintainer_email 'jacob@gitlab.com'
license          'All rights reserved'
description      'Installs/Configures DRBD 8.4.x utilities on Ubuntu 12.04'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.1.9'

supports 'ubuntu', '= 12.04'
