default['gitlab-drbd']['drbd']['version'] = nil
default['gitlab-drbd']['drbd']['repository'] = "git://git.drbd.org/drbd-8.4.git"
default['gitlab-drbd']['drbd']['prefix'] = "/usr/local"
default['gitlab-drbd']['drbd']['localstatedir'] = "/var"
default['gitlab-drbd']['drbd']['sysconfdir'] = "/etc"

default['gitlab-drbd']['resources']['data_bag'] = 'gitlab-drbd-resources'

default['gitlab-drbd']['control_script']['drbd_resource'] = 'gitlab_data'
default['gitlab-drbd']['control_script']['drbd_mountpoint'] = '/var/opt/gitlab'
default['gitlab-drbd']['control_script']['unmount_retries'] = 10
default['gitlab-drbd']['control_script']['floating_ip_address'] = nil
default['gitlab-drbd']['control_script']['floating_ip_netmask'] = nil
default['gitlab-drbd']['control_script']['floating_ip_device'] = 'eth0'
